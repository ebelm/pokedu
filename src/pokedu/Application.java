/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokedu;

/**
 *
 * @author Marius
 */
public class Application {

  public Pokemon pikachu;
  public Pokemon bulbasar;
  public Pokemon charmander;

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    Application app = new Application();
    
    app.pokedex();
  }
  
  public Application() {
    // http://bulbapedia.bulbagarden.net/wiki/Pikachu_(Pok%C3%A9mon)#Game_data
    pikachu = new Pokemon();
    pikachu.name = "Pikachu";
    pikachu.hp = 35;
    pikachu.attack = 55;
    pikachu.defense = 30;

    // http://bulbapedia.bulbagarden.net/wiki/Bulbasaur_(Pok%C3%A9mon)#Game_data
    bulbasar = new Pokemon();
    bulbasar.name = "Bulbasar";
    bulbasar.hp = 45;
    bulbasar.attack = 49;
    bulbasar.defense = 49;

    // http://bulbapedia.bulbagarden.net/wiki/Charmander_(Pok%C3%A9mon)#Game_data
    charmander = new Pokemon();
    charmander.name = "Charmander";
    charmander.hp = 39;
    charmander.attack = 52;
    charmander.defense = 43;
  }

  public void pokedex() {
    System.out.println("==============");
    System.out.println("Pokédex");
    System.out.println("==============");
    
    System.out.println(pikachu.name);
    System.out.println("HP: " + pikachu.hp);
    System.out.println("Attack: " + pikachu.attack);
    System.out.println("Defense: " + pikachu.defense);
    System.out.println("");
    
    System.out.println(bulbasar.name);
    System.out.println("HP: " + bulbasar.hp);
    System.out.println("Attack: " + bulbasar.attack);
    System.out.println("Defense: " + bulbasar.defense);
    System.out.println("");
    
    System.out.println(charmander.name);
    System.out.println("HP: " + charmander.hp);
    System.out.println("Attack: " + charmander.attack);
    System.out.println("Defense: " + charmander.defense);
    System.out.println("==============");
  }
}
